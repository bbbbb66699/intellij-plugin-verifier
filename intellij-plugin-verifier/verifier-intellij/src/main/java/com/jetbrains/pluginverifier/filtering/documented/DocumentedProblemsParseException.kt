package com.jetbrains.pluginverifier.filtering.documented

class DocumentedProblemsParseException(override val message: String) : RuntimeException()