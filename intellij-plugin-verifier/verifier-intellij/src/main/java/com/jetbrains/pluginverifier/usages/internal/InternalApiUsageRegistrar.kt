package com.jetbrains.pluginverifier.usages.internal

interface InternalApiUsageRegistrar {
  fun registerInternalApiUsage(internalApiUsage: InternalApiUsage)
}