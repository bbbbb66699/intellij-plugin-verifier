package com.jetbrains.pluginverifier.usages.nonExtendable

import com.jetbrains.pluginverifier.usages.ApiUsage

/**
 * Usage of `ApiStatus.NonExtendable` API.
 */
abstract class NonExtendableApiUsage : ApiUsage()