package com.jetbrains.pluginverifier.usages.overrideOnly

interface OverrideOnlyRegistrar {
  fun registerOverrideOnlyMethodUsage(overrideOnlyMethodUsage: OverrideOnlyMethodUsage)
}